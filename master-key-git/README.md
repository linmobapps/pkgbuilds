# master-key-git

This is a PKGBUILD to build and install [Master Key](https://gitlab.com/guillermop/master-key/), a password manager application built with Python 3 and GTK that generates and manages passwords without the need to store them. 

