# Experimental PKGBUILDs

This repo contains the PKGBUILD scripts previously included in the linmobapps.frama.io repository. They have all been created with the PINE64 PinePhone in mind, but of course aren't limited to this one device.

This folder contains experimental PKGBUILDs to install software to Arch Linux ARM that's not already in the AUR.

Please use with care and report or fix PKGBUILD related issues. Thank you!

If it works for you and you have checked that the PKGBUILD follows the guidelines, feel free to submit my PKGBUILDs upstream!

PKGBUILDs that don't work yet are in the "WIP" branch.

## How to use this?

Install procedure for Arch/Manjaro users that have not setup an AUR helper yet:
                                                                                                                  
~~~~ 

sudo pacman -Syu base-devel binutils git make gcc
git clone https://framagit.org/linmobapps/pkgbuilds.git
cd pkgbuilds/PKGBUILD-YOU-WANT-TO-BUILD
makepkg -si

~~~~

(For installing an AUR helper read this [old blog post](https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/#preparations).)


## Other sources of Packages/PKGBUILDs that are interesting for PinePhone users/users of ArchLinux ARM on Mobile devices

* [kescher-archpp](https://git.kescher.at/jeremy.kescher/kescher-archpp) ([GitHub mirror](https://github.com/kescherCode/kescher-archpp)), contains a few more packages for use with the PinePhone;
* PrivacyShark: [HowTo](https://privacyshark.zero-credibility.net/#howto), [Packages](https://privacyshark.zero-credibility.net/#packages), [PKGBUILDs](https://gitlab.com/ohfp/pinebookpro-things/-/tree/master), contains privacy centric stuff like Ungoogled Chromium, LibreWolf, Signal Desktop, Axolotl and more;
* [tqre/pinephone-arch](https://github.com/tqre/pinephone-arch), installs p-boot and latest Megi kernel, don't use with Full Disk Encrypted installs now, as it determines the wrong boot partition.

## PKGBUILDs you are likely already using, but might want to have a look at, e.g. for trying out a new release that did not land yet:

* [Arch Linux ARM PKGBUILDs](https://github.com/archlinuxarm/PKGBUILDs),
* [DanctNIX/Arch on Mobile PKGBUILDs](https://github.com/dreemurrs-embedded/Pine64-Arch/tree/master/PKGBUILDS),
* [Manjaro PKGBUILDs](https://gitlab.manjaro.org/packages),
  * [Manjaro ARM PKGBUILDs](https://gitlab.manjaro.org/manjaro-arm/packages), contains highlights like [Lomiri Dev PKGBUILDs](https://gitlab.manjaro.org/manjaro-arm/packages/community/lomiri-devhttps://gitlab.manjaro.org/manjaro-arm/packages/community/lomiri-dev).
